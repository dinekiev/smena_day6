from task_1 import Point
from task_2 import Rectangle


class Rectangle3(Rectangle):
    def __init__(self, top_left, bottom_right):
        self.top_left = Point(top_left.x, top_left.y)
        self.bottom_right = Point(bottom_right.x, bottom_right.y)
