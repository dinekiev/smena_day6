import math


class Circle(object):
    x = 0
    y = 0

    def __init__(self, x, y, radius):
        self.x = x
        self.y = y
        self.radius = radius

    def __str__(self):
        return 'Circle at ({0}, {1}); radius: {2}'.format(self.x, self.y, self.radius)

    def diameter(self):
        return self.radius * 2

    def circumference(self):
        return 2 * math.pi * self.radius

    def area(self):
        return math.pi * self.radius * self.radius
