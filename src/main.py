# coding: utf-8

from task_1 import Point
from task_2 import Rectangle
from task_3 import Rectangle2
from task_4 import Rectangle3
from task_5 import Circle
from task_6 import Student, Course, Lector
from datetime import date

p = Point(13, 27)
print p
print ''

rect = Rectangle(22, 12, 44, 55)
print rect
print 'rect.height(): ', rect.height()
print 'rect.width(): ', rect.width()
print 'rect.perimeter(): ', rect.perimeter()
print 'rect.area(): ', rect.area()
print 'rect.is_square(): ', rect.is_square()
print ''

rect = Rectangle2(11, 11, 30, 30)
print rect
print 'rect.height(): ', rect.height()
print 'rect.width(): ', rect.width()
print 'rect.perimeter(): ', rect.perimeter()
print 'rect.area(): ', rect.area()
print 'rect.is_square(): ', rect.is_square()
print ''

rect = Rectangle3(Point(234, 123), Point(100, 456))
print rect
print 'rect.height():', rect.height()
print 'rect.width():', rect.width()
print 'rect.perimeter():', rect.perimeter()
print 'rect.area():', rect.area()
print 'rect.is_square():', rect.is_square()
print ''

circ = Circle(40, 50, 10)
print circ
print 'circ.diameter(): ', circ.diameter()
print 'circ.circumference(): ', circ.circumference()
print 'circ.area(): ', circ.area()
print ''


courses = []
courses.append(Course('Mathematica', 1))
courses.append(Course('3D Modeling', 2))
courses.append(Course('Networks', 3))

student_list = []
student = Student('Иванов', 'Василий', 'Петрович', date(1990, 5, 5))
student.id = 1
student_list.append(student)
student = Student('Федоров', 'Александр', 'Матвеевич', date(1990, 6, 21))
student.id = 2
student_list.append(student)
student = Student('Едренкин', 'Сергей', 'Павлович', date(1990, 1, 12))
student.id = 3
student_list.append(student)
student = Student('Головина', 'Марина', 'Васильевна', date(1990, 3, 17))
student.id = 4
student_list.append(student)
student = Student('Шишкин', 'Антон', 'Сергеевич', date(1990, 4, 27))
student.id = 5
student_list.append(student)
student = Student('Решетников', 'Михаил', 'Анатольевич', date(1990, 8, 6))
student.id = 6
student_list.append(student)
student = Student('Любимцева', 'Надя', 'Александровна', date(1990, 7, 23))
student.id = 7
student_list.append(student)


courses[0].sign_in_student(student_list[0])
courses[0].sign_in_student(student_list[1])
courses[1].sign_in_student(student_list[2])
courses[1].sign_in_student(student_list[3])
courses[1].sign_in_student(student_list[0])
courses[1].sign_in_student(student_list[1])
courses[2].sign_in_student(student_list[4])
courses[2].sign_in_student(student_list[5])
courses[2].sign_in_student(student_list[6])
for course in courses:
    print course
    print ''

for student in student_list:
    print student
print ''

lector = Lector('Беляков Аркадий Васильевич', courses[0])
print 'Преподаватель ' + lector.name +' - ' + lector.course.name
