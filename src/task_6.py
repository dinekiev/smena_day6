# coding: utf-8
from datetime import date


class Student:
    def __init__(self, lname, fname, mname, date_birth):
        self.fname = fname
        self.lname = lname
        self.mname = mname
        self.date_birth = date_birth
        self.id = 0
        self.courses = []

    def info(self):
        return self.lname + ' ' + self.lname + ' ' + self.mname + \
                ', Дата рождения: ' + self.date_birth.strftime("%d %B %Y") + \
                ', Студенчекий билет №: {0}'.format(self.id) + \
                ', Курсов: {0}'.format(len(self.courses))

    def sign_to_course(self, course):
        self.courses.append(course)

    def __str__(self):
        s = self.info()
        if len(self.courses) > 0:
            s = s + '\nЗаписан на курсы:'
        for course in self.courses:
            s = s + '\n\t{0}, аудитория: {1}'.format(course.name, course.room)
        return s


class Course(object):
    def __init__(self, name='', room=0):
        self.name = name
        self.room = room
        self.students = []

    def count_students(self):
        return len(self.students)

    def __str__(self):
        s = '{0}, room: {1}, count of students: {2}'.format(self.name, self.room, self.count_students())
        if len(self.students) > 0:
            s = s + '\nСписок записанных студентов:'
        for student in self.students:
            s = s + '\n\t' + student.info()
        return s

    def sign_in_student(self, student):
        self.students.append(student)
        student.sign_to_course(self)

    def set_lector(self, lector):
        self.lector = lector


class Lector:
    def __init__(self, name, course):
        self.name = name
        self.course = course
        course.set_lector(self)
