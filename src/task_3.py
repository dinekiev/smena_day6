from task_1 import Point
from task_2 import Rectangle


class Rectangle2(Rectangle):
    def __init__(self, top, left, width, height):
        assert width >= 0, 'value of width can not be < 0'
        assert height >= 0, 'value of height can not be < 0'
        self.top_left = Point(left, top)
        self.bottom_right = Point(top + height, left + width)

    def __str__(self):
        return 'Rectangle at {0}, {1}; height: {2}, width: {3}'.format(self.top_left.x, self.top_left.y,
                                                                       self.height(), self.width())