from task_1 import Point


class Rectangle(object):
    def __init__(self, top, left, bottom, right):
        self.top_left = Point(left, top)
        self.bottom_right = Point(right, bottom)

    def __str__(self):
        return 'Rectangle at ({0},{1}), ({2},{3})'.format(self.top_left.x, self.top_left.y,
                                                         self.bottom_right.x, self.bottom_right.y)

    def width(self):
        return abs(self.top_left.x - self.bottom_right.x)

    def height(self):
        return abs(self.top_left.y - self.bottom_right.y)

    def perimeter(self):
        return (self.width() + self.height()) * 2

    def area(self):
        return self.width() * self.height()

    def is_square(self):
        return self.width() == self.height()
